let trainer = {
    name: "Ash Ketchum",
    age: 10,
    friends: {
        Kanto: ["Brock", "Misty"],
        Hoenn: ["May", "Max"],
    },
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    talk: function() {
        console.log("Pikachu, I choose you!");
    }
};

function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = level*2;
    this.attack = level;
    this.tackle = function(target){
        console.log(`${this.name} attacked ${target.name}!`);
        console.log(`${target.name}'s health is now reduced to ` + (target.health - this.attack));
        target.health = (target.health-this.attack);
        if(target.health <= 0) {
            console.log(`${target.name} fainted.`)
            }
        };
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
console.log(trainer.talk());
let Pikachu = new Pokemon("Pikachu",12);
console.log(Pikachu);
let Geodude = new Pokemon("Geodude", 8);
console.log(Geodude);
let MewTwo = new Pokemon("Mewtwo", 100);
console.log(MewTwo);

console.log(Pikachu.tackle(MewTwo));
console.log(MewTwo);
console.log(MewTwo.tackle(Geodude));
console.log(Geodude);